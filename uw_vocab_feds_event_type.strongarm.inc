<?php

/**
 * @file
 * uw_vocab_feds_event_type.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uw_vocab_feds_event_type_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'metatag_enable_taxonomy_term__feds_event_types';
  $strongarm->value = TRUE;
  $export['metatag_enable_taxonomy_term__feds_event_types'] = $strongarm;

  return $export;
}
